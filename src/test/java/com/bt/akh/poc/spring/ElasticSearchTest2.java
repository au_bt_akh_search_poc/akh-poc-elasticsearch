package com.bt.akh.poc.spring;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bt.akh.poc.crawler.model.mdh.GetEquityResult;
import com.bt.akh.poc.crawler.service.GetJSONResultService;
import com.bt.akh.poc.crawler.service.impl.EquityServiceImpl;
import com.bt.akh.poc.spring.config.Config;
import com.bt.akh.poc.spring.model.Announcement;
import com.bt.akh.poc.spring.model.ListedEquity;
import com.bt.akh.poc.spring.model.PolicyDocuments;
import com.bt.akh.poc.spring.model.ResearchReport;
import com.bt.akh.poc.spring.model.WebArticles;
import com.bt.akh.poc.spring.service.ListedEquityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class }, loader = AnnotationConfigContextLoader.class)
public class ElasticSearchTest2 {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private ListedEquityService listedEquityService;

    private EquityServiceImpl getEquityResultService;
    
    private GetEquityResult getEquityResult;

    @Before
    public void before() {
    	
        elasticsearchTemplate.deleteIndex(ListedEquity.class);
        elasticsearchTemplate.createIndex(ListedEquity.class);       
        elasticsearchTemplate.deleteIndex(Announcement.class);
        elasticsearchTemplate.createIndex(Announcement.class);
        elasticsearchTemplate.deleteIndex(WebArticles.class);       
        elasticsearchTemplate.createIndex(WebArticles.class);
        elasticsearchTemplate.deleteIndex(PolicyDocuments.class);        
        elasticsearchTemplate.createIndex(PolicyDocuments.class);
        elasticsearchTemplate.deleteIndex(ResearchReport.class);
        elasticsearchTemplate.createIndex(ResearchReport.class);
                       
        getEquityResultService = new EquityServiceImpl();

    }
    
    @Test
    public void givenArticleService_whenSaveArticle_thenIdIsAssigned() {
    	getEquityResult = getEquityResultService.get(GetJSONResultService.restTemplate, "BHP").GetEquityResult;
    	
    	ListedEquity announcement = new ListedEquity(getEquityResult.getStockName(),getEquityResult.getStockCode(), new Date());
    	announcement = listedEquityService.save(announcement);
        assertNotNull(announcement.getId());
    }
}