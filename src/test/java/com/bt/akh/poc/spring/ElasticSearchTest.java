package com.bt.akh.poc.spring;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bt.akh.poc.spring.config.Config;
import com.bt.akh.poc.spring.model.Announcement;
import com.bt.akh.poc.spring.service.AnnouncementService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class }, loader = AnnotationConfigContextLoader.class)
public class ElasticSearchTest {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private AnnouncementService announcementService;

    

    @Before
    public void before() {
    	
        elasticsearchTemplate.deleteIndex(Announcement.class);
        
        elasticsearchTemplate.createIndex(Announcement.class);
        
        Announcement announcement1 = new Announcement("Vamos 1"); 
        //announcement1.setDefaultFormat("e1xydGYxXGFuc2kNCkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0DQpccGFyIH0=");
        
        //announcementService.save(announcement1);

        Announcement announcement2 = new Announcement("Vamos 2");        
        
        //announcementService.save(announcement);

        Announcement announcement3 = new Announcement("Vamos 3");
       
        announcementService.save(asList(announcement1,announcement2,announcement3));

    }
    
    @Test
    public void givenArticleService_whenSaveArticle_thenIdIsAssigned() {
    	Announcement announcement = new Announcement("Vamos 4");
    	announcement = announcementService.save(announcement);
        assertNotNull(announcement.getId());
    }
}