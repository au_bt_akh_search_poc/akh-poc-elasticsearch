package com.bt.akh.poc.crawler;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bt.akh.poc.crawler.service.impl.EquityServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class GetEquityResultTest {

    private EquityServiceImpl getEquityResultService;
    

    @Before
    public void before() {
    	getEquityResultService = new EquityServiceImpl();
    }
    
	@Test
	public void testGet() {
		assertNotNull(getEquityResultService.get(getEquityResultService.restTemplate, "BHP"));		
	}

}
