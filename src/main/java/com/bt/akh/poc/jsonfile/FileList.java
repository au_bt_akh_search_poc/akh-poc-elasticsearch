package com.bt.akh.poc.jsonfile;

import java.util.List;

import com.bt.akh.poc.crawler.model.mdh.GetPolicyDocumentsResult;

public class FileList {
	private String success;
	private int results;
	private int total;
	private int	offset;
	private List<GetPolicyDocumentsResult> hits;
	
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public int getResults() {
		return results;
	}
	public void setResults(int results) {
		this.results = results;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public List<GetPolicyDocumentsResult> getHits() {
		return hits;
	}
	public void setHits(List<GetPolicyDocumentsResult> hits) {
		this.hits = hits;
	}
	
}
