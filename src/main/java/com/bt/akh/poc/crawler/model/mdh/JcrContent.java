package com.bt.akh.poc.crawler.model.mdh;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JcrContent {
	
	
	
	public JcrContent() {
		super();
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("metadata")
	private MetadataPolicyDocument metadata;

	public MetadataPolicyDocument getMetadata() {
		return metadata;
	}

	public void setMetadata(MetadataPolicyDocument metadata) {
		this.metadata = metadata;
	}
	
	

}
