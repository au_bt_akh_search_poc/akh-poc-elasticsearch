package com.bt.akh.poc.crawler.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bt.akh.poc.crawler.model.mdh.EquityJSON;
import com.bt.akh.poc.crawler.service.GetJSONResultService;

@Service
public class EquityServiceImpl implements GetJSONResultService {
	
	//private String requestPath = getEquityRequestPath;
	private String requestPath ;
	private String listPath ;
	
	public EquityServiceImpl (){
		
	}
	
	public EquityServiceImpl (String mdhEndPointURL){
		this.requestPath = mdhEndPointURL + getEquityRequestPath;
		this.listPath = mdhEndPointURL + GetEquitiesStockCodesRequestPath;
	}
	
	
	@Override
	public EquityJSON get(RestTemplate restTemplate, String key){
		log.info("inside Get with parameter: "+key );
		EquityJSON equity = restTemplate.getForObject(requestPath+key, EquityJSON.class);		
		log.info(equity.toString());
		return equity;
	}
	
	//@Override
	public List<String> getList(RestTemplate restTemplate){

		List<String> stockCodes = new ArrayList<String>();
		
		String s = restTemplate.getForObject(listPath, String.class);
		log.info("s="+s.toString());

		s = s.substring(s.indexOf("[")+2);
		s = s.replace("\"]}","");
		stockCodes = Arrays.asList(s.split("\",\""));
		log.info(stockCodes.toString());
//		log.info(stockCodes.get(0));
//		log.info(stockCodes.get(stockCodes.size()-2));
//		log.info(stockCodes.get(stockCodes.size()-1));

		return stockCodes;
	}
}
