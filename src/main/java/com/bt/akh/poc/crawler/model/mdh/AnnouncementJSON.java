package com.bt.akh.poc.crawler.model.mdh;

public class AnnouncementJSON extends BaseResultJSON {


    public GetAnnouncementsPagedResult GetAnnouncementsPagedResult;

    public AnnouncementJSON() {
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "GetAnnouncementsPagedResult=" + GetAnnouncementsPagedResult +
                '}';
    }
    
    
}
