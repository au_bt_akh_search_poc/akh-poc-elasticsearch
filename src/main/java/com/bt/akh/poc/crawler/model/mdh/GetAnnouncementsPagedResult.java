package com.bt.akh.poc.crawler.model.mdh;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAnnouncementsPagedResult {

	public GetAnnouncementsPagedResult(){}
	
	@JsonProperty("Code")
	private String code;
	
	@JsonProperty("Data")
	private List<GetAnnouncementResult> data;
	
	@JsonProperty("Message")
	private String message;

	@JsonProperty("Success")
	private String Success;

	@JsonProperty("TotalRecords")
	private String TotalRecords;
	
	public String getCode() {
		return code;
	}

	public List<GetAnnouncementResult> getData() {
		return data;
	}

	public String getMessage() {
		return message;
	}

	public String getSuccess() {
		return Success;
	}

	public String getTotalRecords() {
		return TotalRecords;
	}

	@Override
	public String toString() {
		return "GetAnnouncementPageResult [code=" + code + ", message=" + message + ", Success="
				+ Success + ", TotalRecords=" + TotalRecords + "]";
	}



	
	

	
	
    
    
}
