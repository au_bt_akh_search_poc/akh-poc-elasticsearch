package com.bt.akh.poc.crawler.service;

//import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.bt.akh.poc.crawler.model.mdh.BaseResultJSON;
//import com.bt.akh.poc.crawler.model.mdh.EquityJSON;
//import com.bt.akh.poc.crawler.service.impl.GetEquityResultServiceImpl;

public interface GetJSONResultService {

	public static final Logger log = LoggerFactory.getLogger(GetJSONResultService.class);
	//public static final String getEquityRequestPath = "http://52.65.110.187:83/MS_Equities.svc/getEquity/BHP";
	
	public static final String getEquityRequestPath = "/MS_Equities.svc/getEquity/";
	public static final String GetEquitiesStockCodesRequestPath = "/MS_Equities.svc/getEquitiesStockCodes";
	public static final String getAnnouncementRequestPath = "/GetAnnouncementsPaged/0/10/true/";
	public static final String getPolicyDocumentsRequestPath =  "/akh-btap-data/data.json";
	public static final RestTemplate restTemplate = new RestTemplate();
	
//	public BaseResultJSON get(RestTemplate restTemplate);
	
	
	public BaseResultJSON get(RestTemplate restTemplate, String key);
	
	
		
	
}
