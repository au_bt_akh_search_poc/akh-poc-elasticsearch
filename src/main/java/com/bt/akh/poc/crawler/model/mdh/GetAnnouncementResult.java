package com.bt.akh.poc.crawler.model.mdh;

//import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAnnouncementResult {

	public GetAnnouncementResult(){}
	
	@JsonProperty("AnalystNote")
	private String analystNote;
	
	@JsonProperty("AnnoucementAction")
	private String annoucementAction;
	
	@JsonProperty("AnnouncementDate")
	private String announcementDate;

	@JsonProperty("CompanyName")
	private String companyName;

	@JsonProperty("EventType")
	private String eventType;

	@JsonProperty("ExchangeCode")
	private String exchangeCode;

	@JsonProperty("Heading")
	private String heading;

	@JsonProperty("LastPrice")
	private String lastPrice;
	
	@JsonProperty("ID")
	private String id;

	@JsonProperty("Recommendation")
	private String recommendation;

	@JsonProperty("RecommendationChange")
	private String recommendationChange;

	@JsonProperty("SecurityID")
	private String securityID;
	
	@JsonProperty("SecurityType")
	private String securityType;

	@JsonProperty("StockCode")
	private String stockCode;	

	
    


	public String getAnalystNote() {
		return analystNote;
	}





	public String getAnnoucementAction() {
		return annoucementAction;
	}





	public String getAnnouncementDate() {
		return announcementDate;
	}





	public String getCompanyName() {
		return companyName;
	}





	public String getEventType() {
		return eventType;
	}





	public String getExchangeCode() {
		return exchangeCode;
	}





	public String getHeading() {
		return heading;
	}





	public String getLastPrice() {
		return lastPrice;
	}





	public String getId() {
		return id;
	}





	public String getRecommendation() {
		return recommendation;
	}





	public String getRecommendationChange() {
		return recommendationChange;
	}





	public String getSecurityID() {
		return securityID;
	}





	public String getSecurityType() {
		return securityType;
	}





	public String getStockCode() {
		return stockCode;
	}





	@Override
	public String toString() {
		return "GetAnnouncementResult [analystNote=" + analystNote + ", annoucementAction=" + annoucementAction
				+ ", announcementDate=" + announcementDate + ", companyName=" + companyName + ", eventType=" + eventType
				+ ", exchangeCode=" + exchangeCode + ", heading=" + heading + ", lastPrice=" + lastPrice
				+ ", recommendation=" + recommendation + ", recommendationChange=" + recommendationChange
				+ ", securityID=" + securityID + ", securityType=" + securityType + ", stockCode=" + stockCode + "]";
	}

	
    
    
}
