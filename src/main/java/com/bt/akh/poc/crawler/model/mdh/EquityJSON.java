package com.bt.akh.poc.crawler.model.mdh;

public class EquityJSON extends BaseResultJSON {


    public GetEquityResult GetEquityResult;

    public EquityJSON() {
    }

    @Override
    public String toString() {
        return "Equity{" +
                ", GetEquityResult=" + GetEquityResult +
                '}';
    }
}
