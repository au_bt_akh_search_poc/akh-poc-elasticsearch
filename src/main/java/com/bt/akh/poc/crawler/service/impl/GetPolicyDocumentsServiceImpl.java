package com.bt.akh.poc.crawler.service.impl;



import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bt.akh.poc.crawler.model.mdh.PolicyDocumentsJSON;
//import com.bt.akh.poc.crawler.model.mdh.EquityJSON;
import com.bt.akh.poc.crawler.service.GetJSONResultService;

@Service
public class GetPolicyDocumentsServiceImpl implements GetJSONResultService {
	
	//private String requestPath = getEquityRequestPath;
	private String requestPath ;
	
	public GetPolicyDocumentsServiceImpl (){
		
	}
	
	public GetPolicyDocumentsServiceImpl (String mdhEndPointURL){
		this.requestPath = mdhEndPointURL + getPolicyDocumentsRequestPath;// getAnnouncementRequestPath;
		log.info("getPolicyDocument requestPath: "+this.requestPath);
	}	
	
	@Override
	public PolicyDocumentsJSON get(RestTemplate restTemplate, String key){
		//AnnouncementJSON announcement = restTemplate.getForObject(requestPath, AnnouncementJSON.class);
		log.info("Entro en");
		ResponseEntity<PolicyDocumentsJSON> responseEntity = restTemplate.getForEntity(requestPath, PolicyDocumentsJSON.class);
			
		log.info("responseEntity:"+responseEntity.toString());
		return responseEntity.getBody();
	}	
}