package com.bt.akh.poc.crawler.model.mdh;

//import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetPolicyDocumentsResult {

	public GetPolicyDocumentsResult(){}
	
	@JsonProperty("jcr:path")
	private String jcrPath;
	
	@JsonProperty("jcr:content")
	private JcrContent jcrContent;

	public String getJcrPath() {
		return jcrPath;
	}

	public void setJcrPath(String jcrPath) {
		this.jcrPath = jcrPath;
	}

	public JcrContent getJcrContent() {
		return jcrContent;
	}

	public void setJcrContent(JcrContent jcrContent) {
		this.jcrContent = jcrContent;
	}
	
	
	
    
    
}
