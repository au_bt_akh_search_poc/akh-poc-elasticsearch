package com.bt.akh.poc.crawler.model.mdh;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetadataPolicyDocument {

	public MetadataPolicyDocument() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@JsonProperty("dc:description")
	private String dcDescription;
	
	@JsonProperty("dc:title")
	private String dcTitle;

	public String getDcDescription() {
		return dcDescription;
	}

	public void setDcDescription(String dcDescription) {
		this.dcDescription = dcDescription;
	}

	public String getDcTitle() {
		return dcTitle;
	}

	public void setDcTitle(String dcTitle) {
		this.dcTitle = dcTitle;
	}
	
	

}
