package com.bt.akh.poc.crawler.model.mdh;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetEquityResult {

	public GetEquityResult(){}
	
	@JsonProperty("CompanyProfile")
	private String CompanyProfile;
	
	@JsonProperty("FairValue")
	private String fairValue;
	
	@JsonProperty("FairValueUncertainty")
	private String FairValueUncertainty;

	@JsonProperty("InvestmentPerspective")
	private String InvestmentPerspective;

	@JsonProperty("PriceDate")
	private String PriceDate;

	@JsonProperty("Profile")
	private String Profile;

	@JsonProperty("Recommendation")
	private String Recommendation;

	@JsonProperty("ResearchReport")
	private String ResearchReport;

	@JsonProperty("ResearchSnapshot")
	private String ResearchSnapshot;

	@JsonProperty("Sector")
	private String Sector;

	@JsonProperty("SecurityID")
	private String SecurityID;

	@JsonProperty("StockCode")
	private String StockCode;

	@JsonProperty("StockName")
	private String StockName;

	@JsonProperty("YesterdaysClosePrice")
	private String YesterdaysClosePrice;


	
    public String getStockCode() {
		return StockCode;
	}




	public String getStockName() {
		return StockName;
	}

	public String getProfile() {
		return Profile;
	}

	public String getInvestmentPerspective() {
		return InvestmentPerspective;
	}


	@Override
    public String toString() {
        return "Value{" +
                "CompanyProfile=" + CompanyProfile +
                ", FairValue='" + fairValue + '\'' +
                ", FairValueUncertainty='" + FairValueUncertainty + '\'' +
                ", InvestmentPerspective='" + InvestmentPerspective + '\'' +
                ", PriceDate='" + PriceDate + '\'' +
                ", Profile='" + Profile + '\'' +
                ", Recommendation='" + Recommendation + '\'' +
                ", ResearchReport='" + ResearchReport + '\'' +
                ", ResearchSnapshot='" + ResearchSnapshot + '\'' +
                ", Sector='" + Sector + '\'' +
                ", SecurityID='" + SecurityID + '\'' +
                ", StockCode='" + StockCode + '\'' +
                ", StockName='" + StockName + '\'' +
                ", YesterdaysClosePrice='" + YesterdaysClosePrice + '\'' +
                '}';
    }
}
