package com.bt.akh.poc.crawler.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bt.akh.poc.crawler.model.mdh.AnnouncementJSON;
//import com.bt.akh.poc.crawler.model.mdh.EquityJSON;
import com.bt.akh.poc.crawler.service.GetJSONResultService;

@Service
public class GetAnnouncementResultServiceImpl implements GetJSONResultService {
	
	//private String requestPath = getEquityRequestPath;
	private String requestPath ;
	
	public GetAnnouncementResultServiceImpl (){
		
	}
	
	public GetAnnouncementResultServiceImpl (String mdhEndPointURL){
		this.requestPath = mdhEndPointURL + getAnnouncementRequestPath;
		log.info("Announcement  requestPath: " +this.requestPath);
	}	
	
	@Override
	public AnnouncementJSON get(RestTemplate restTemplate, String key){
		//AnnouncementJSON announcement = restTemplate.getForObject(requestPath, AnnouncementJSON.class);
		ResponseEntity<AnnouncementJSON> responseEntity = restTemplate.getForEntity(requestPath, AnnouncementJSON.class);
			
		log.info(responseEntity.toString());
		return responseEntity.getBody();
	}	
	
	public List<AnnouncementJSON> getList(RestTemplate restTemplate){
		AnnouncementJSON[] forNow = restTemplate.getForObject(requestPath, AnnouncementJSON[].class);
	    return Arrays.asList(forNow);
		
	}	
}