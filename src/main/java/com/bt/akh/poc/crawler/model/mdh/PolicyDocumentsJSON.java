package com.bt.akh.poc.crawler.model.mdh;

import java.util.List;

public class PolicyDocumentsJSON extends BaseResultJSON {

	public boolean success;
	
	public String results;
	
	public String total;
	
	public String offset;
	
    public List<GetPolicyDocumentsResult> hits;

    public PolicyDocumentsJSON() {
    }

   
    
}
