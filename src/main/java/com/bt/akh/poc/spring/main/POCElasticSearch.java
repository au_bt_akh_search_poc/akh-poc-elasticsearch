package com.bt.akh.poc.spring.main;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

//import com.bt.akh.poc.crawler.model.mdh.AnnouncementJSON;
import com.bt.akh.poc.crawler.model.mdh.GetAnnouncementsPagedResult;
import com.bt.akh.poc.crawler.model.mdh.GetAnnouncementResult;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import com.bt.akh.poc.crawler.model.mdh.GetEquityResult;
import com.bt.akh.poc.crawler.service.GetJSONResultService;

import com.bt.akh.poc.crawler.service.impl.EquityServiceImpl;

import com.bt.akh.poc.crawler.service.impl.GetAnnouncementResultServiceImpl;
//import com.bt.akh.poc.crawler.service.impl.GetEquityResultServiceImpl;

import com.bt.akh.poc.spring.config.Config;
import com.bt.akh.poc.spring.model.Announcement;
import com.bt.akh.poc.spring.model.ListedEquity;
import com.bt.akh.poc.spring.service.AnnouncementService;
import com.bt.akh.poc.spring.service.ListedEquityService;

public class POCElasticSearch {
	
	//@Autowired
	//private static ElasticsearchTemplate elasticsearchTemplate;
	
	@Autowired
	private static ListedEquityService listedEquityService;
	

	private EquityServiceImpl getEquityResultService;
	
	public POCElasticSearch() {
		super();
		getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
	}


	@Autowired
	private static AnnouncementService announcementService;
	
	//private GetEquityResultServiceImpl getEquityResultService;
	
	private GetAnnouncementResultServiceImpl getAnnouncementResultService;
	

	public static final Logger log = LoggerFactory.getLogger(POCElasticSearch.class );
	
	GetEquityResult getEquityResult;
	
	//List<AnnouncementJSON> getAnnouncementResultList;
	
	GetAnnouncementsPagedResult getAnnouncementPageResult;
	
	private static String mdhEndPointURL;
	
	private static String mdhEndPointAnnouncement;
	
	private static String key2;
	
	
	public static void main(String[] args) throws URISyntaxException, Exception {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		try {			
			//ApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
			ctx.register(Config.class);
			ctx.refresh();
			listedEquityService = ctx.getBean(ListedEquityService.class);

			log.info("Connected to Elasticsearch");

			announcementService = ctx.getBean(AnnouncementService.class);
			
			
			mdhEndPointURL  = ctx.getBeanFactory().resolveEmbeddedValue("${mdhEndPoint.url}");
			
			log.info("mdhEndPointIp: "+ mdhEndPointURL);
			
			mdhEndPointAnnouncement  = ctx.getBeanFactory().resolveEmbeddedValue("${mdhEndPointAnnouncement.url}");
			
			log.info("After getBeanFactory() mdhEndPointAnnouncement ");
			
			POCElasticSearch p = new POCElasticSearch();
			
			log.info("After POCElasticSearch p ");
			
			p.getEquity("BHP");

			List<String> equitList = p.getEquityList();
/*			
			log.info("After p.getEquityList(); ");
			
			for(String s:equitList){
				p.getEquity(s);
				p.addListedEquity();
			}
			
*/
			
			p.addListedEquityBulk (equitList, p);
			
			
			log.info("After addListedEquityBulk");
			
			p.getAnnouncementPage();
			
			log.info("After getAnnouncementPage");

			p.addAnnouncement();
			
			log.info("*** After all. ****");
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally {
			log.info("Finally begin");
			ctx.close();
			log.info("Finally final");
		}
		
		log.info("Finish");
	
	}
	

	public void getEquity(String key) {
//		getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
		getEquityResult = getEquityResultService.get(GetJSONResultService.restTemplate, key).GetEquityResult;		
		
	}
	
	public List<String> getEquityList( ) {
//		getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
		List<String> equitList = getEquityResultService.getList(GetJSONResultService.restTemplate);		
		return equitList;
	}
	
/*	
	public void addListedEquity() {
		
//		ListedEquity listedEquity = new ListedEquity(getEquityResult.getStockName(), getEquityResult.getStockCode(), new Date());
		ListedEquity listedEquity = new ListedEquity(
				getEquityResult.getStockName()
				, getEquityResult.getStockCode()
				, new Date()
				, getEquityResult.getProfile() + getEquityResult.getInvestmentPerspective() 
				);
		
		listedEquityService.save(listedEquity);
	}
	*/
	
	private void addListedEquityBulk( List<String> equitList, POCElasticSearch PES ) {
		
		List<ListedEquity> listedEquityList = new ArrayList<ListedEquity>();
		
		log.info("Inside addListedEquityBulk; ");
		
		for(String s:equitList){
			PES.getEquity(s);
			listedEquityList.add(
								new ListedEquity(
								getEquityResult.getStockName()
								, getEquityResult.getStockCode()
								, new Date()
								, getEquityResult.getProfile() + getEquityResult.getInvestmentPerspective() 
								,"/marketData/listedEquities/" + getEquityResult.getStockCode()
								,"http://akh.web.btfin.com/research/equities/"  + getEquityResult.getStockCode()
								)
					     );
		}
		listedEquityService.save(listedEquityList);
		
		log.info("After getEquityList");
		
	}


	private void getAnnouncementPage() {
		// TODO Auto-generated method stub
		getAnnouncementResultService = new GetAnnouncementResultServiceImpl(mdhEndPointAnnouncement);
		getAnnouncementPageResult = getAnnouncementResultService.get(GetJSONResultService.restTemplate, key2).GetAnnouncementsPagedResult;
		
	}
	
	private void addAnnouncement() {
		// TODO Auto-generated method stub
		List<Announcement> announcementList = new ArrayList<Announcement>();
	
		for (GetAnnouncementResult temp : getAnnouncementPageResult.getData()) {
			log.info(temp.toString());
			announcementList.add(new Announcement(temp.getStockCode(), temp.getCompanyName(),temp.getEventType(),temp.getHeading(), temp.getId()));
		}
		announcementService.save(announcementList);
	}

	/*private void getAnnouncementList() {
		// TODO Auto-generated method stub
		getAnnouncementResultService = new GetAnnouncementResultServiceImpl(mdhEndPointAnnouncement);
		getAnnouncementResultList = getAnnouncementResultService.getList(GetJSONResultService.restTemplate);
		
	}*/


}
