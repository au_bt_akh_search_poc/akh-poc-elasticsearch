package com.bt.akh.poc.spring.model;

import java.util.Arrays;
import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "commonsearch", type = "announcement")
public class Announcement extends DocumentEntityGeneric {

	
	public Announcement() {
    }
	
	public Announcement(String article_address) {		
		this.article_address = article_address;
	}
	
	public Announcement(String article_stock_code, String article_company_name,
			String cms_event_type, String cms_heading, String cms_id) {
		super();
		
		this.article_stock_code = article_stock_code;
		this.article_company_name = article_company_name;
		this.cms_event_type = cms_event_type;
		this.cms_heading = cms_heading;
		this.cms_id = cms_id;
	}
	
	
	
	public Announcement(String article_address, String article_author, String article_brand,
			Integer article_num_of_pages, String article_phonenum_alfa, Date article_publish_datetime,
			String article_stock_name, String article_stock_code, String article_website, String article_company_name,
			String article_body_content, String cms_contributor, String cms_copyright_owner, String cms_copyright,
			String cms_created_with, String cms_creator, String cms_description, String cms_event_type,
			Date cms_expiry_datetime, String cms_heading, String cms_id, String cms_language, Date cms_off_time,
			Date cmd_on_time, String[] cms_tags, String cms_title, String cms_type, String cms_usage_restriction,
			String cms_usage_terms, String cms_version, String resource_author, String resource_body_content,
			boolean resouce_contains_audio, String resource_file_name, Date resource_lastupdated_datetime,
			Integer resource_number_of_bytes, Date resource_publish_datetime, String resource_title,
			String resource_uri, String resource_url, String resource_base64_file) {
		super();
		this.article_address = article_address;
		this.article_author = article_author;
		this.article_brand = article_brand;
		this.article_num_of_pages = article_num_of_pages;
		this.article_phonenum_alfa = article_phonenum_alfa;
		this.article_publish_datetime = article_publish_datetime;
		this.article_stock_name = article_stock_name;
		this.article_stock_code = article_stock_code;
		this.article_website = article_website;
		this.article_company_name = article_company_name;
		this.article_body_content = article_body_content;
		this.cms_contributor = cms_contributor;
		this.cms_copyright_owner = cms_copyright_owner;
		this.cms_copyright = cms_copyright;
		this.cms_created_with = cms_created_with;
		this.cms_creator = cms_creator;
		this.cms_description = cms_description;
		this.cms_event_type = cms_event_type;
		this.cms_expiry_datetime = cms_expiry_datetime;
		this.cms_heading = cms_heading;
		this.cms_id = cms_id;
		this.cms_language = cms_language;
		this.cms_off_time = cms_off_time;
		this.cmd_on_time = cmd_on_time;
		this.cms_tags = cms_tags;
		this.cms_title = cms_title;
		this.cms_type = cms_type;
		this.cms_usage_restriction = cms_usage_restriction;
		this.cms_usage_terms = cms_usage_terms;
		this.cms_version = cms_version;
		this.resource_author = resource_author;
		this.resource_body_content = resource_body_content;
		this.resouce_contains_audio = resouce_contains_audio;
		this.resource_file_name = resource_file_name;
		this.resource_lastupdated_datetime = resource_lastupdated_datetime;
		this.resource_number_of_bytes = resource_number_of_bytes;
		this.resource_publish_datetime = resource_publish_datetime;
		this.resource_title = resource_title;
		this.resource_uri = resource_uri;
		this.resource_url = resource_url;
		this.resource_base64_file = resource_base64_file;
	}

	

	@Override
	public String toString() {
		return "Announcement [article_address=" + article_address + ", article_author=" + article_author
				+ ", article_brand=" + article_brand + ", article_num_of_pages=" + article_num_of_pages
				+ ", article_phonenum_alfa=" + article_phonenum_alfa + ", article_publish_datetime="
				+ article_publish_datetime + ", article_stock_name=" + article_stock_name + ", article_stock_code="
				+ article_stock_code + ", article_website=" + article_website + ", article_company_name="
				+ article_company_name + ", article_body_content=" + article_body_content + ", cms_contributor="
				+ cms_contributor + ", cms_copyright_owner=" + cms_copyright_owner + ", cms_copyright=" + cms_copyright
				+ ", cms_created_with=" + cms_created_with + ", cms_creator=" + cms_creator + ", cms_description="
				+ cms_description + ", cms_event_type=" + cms_event_type + ", cms_expiry_datetime="
				+ cms_expiry_datetime + ", cms_heading=" + cms_heading + ", cms_id=" + cms_id + ", cms_language="
				+ cms_language + ", cms_off_time=" + cms_off_time + ", cmd_on_time=" + cmd_on_time + ", cms_tags="
				+ Arrays.toString(cms_tags) + ", cms_title=" + cms_title + ", cms_type=" + cms_type
				+ ", cms_usage_restriction=" + cms_usage_restriction + ", cms_usage_terms=" + cms_usage_terms
				+ ", cms_version=" + cms_version + ", resource_author=" + resource_author + ", resource_body_content="
				+ resource_body_content + ", resouce_contains_audio=" + resouce_contains_audio + ", resource_file_name="
				+ resource_file_name + ", resource_lastupdated_datetime=" + resource_lastupdated_datetime
				+ ", resource_number_of_bytes=" + resource_number_of_bytes + ", resource_publish_datetime="
				+ resource_publish_datetime + ", resource_title=" + resource_title + ", resource_uri=" + resource_uri
				+ ", resource_url=" + resource_url + "]";
	}
}
