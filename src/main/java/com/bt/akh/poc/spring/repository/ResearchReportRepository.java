package com.bt.akh.poc.spring.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.bt.akh.poc.spring.model.ResearchReport;

@Repository
public interface ResearchReportRepository extends ElasticsearchRepository<ResearchReport, String> {

   }
