package com.bt.akh.poc.spring.main;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;


public class POCESMain {
	
	private int repeatCount = 3;

	public static void main(String[] args) throws SchedulerException, InterruptedException {

		
		POCESMain p = new POCESMain();	
		p.fireJob();
		
/*		
		listedEquityService = ctx.getBean(ListedEquityService.class);
		p.getEquity();
		p.addListedEquity();*/
	
	}
	
	public void fireJob() throws SchedulerException, InterruptedException {
		SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
		Scheduler scheduler = schedFact.getScheduler();
		scheduler.start();

		JobBuilder jobBuilder = JobBuilder.newJob(IndexingJob.class);
		JobDetail jobDetail = jobBuilder.withIdentity("repeatSearchJob", "group1").build();
		
		// Trigger the job to run now, and then every 15 seconds
		Trigger trigger = TriggerBuilder.newTrigger()
		.withIdentity("searchTrigger", "group1")
		.startNow()
		.withSchedule(SimpleScheduleBuilder.simpleSchedule()
                .withRepeatCount(repeatCount)
                .withIntervalInSeconds(15))		
		.build();
		

		// Tell quartz to schedule the job using our trigger
		scheduler.scheduleJob(jobDetail, trigger);
	}

	

}
