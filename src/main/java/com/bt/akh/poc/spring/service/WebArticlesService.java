package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.model.WebArticles;

public interface WebArticlesService {
	
	WebArticles save(WebArticles article);

	WebArticles findOne(String id);

    Iterable<WebArticles> findAll();

    long count();

    void delete(WebArticles article);
}
