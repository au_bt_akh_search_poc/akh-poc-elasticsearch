package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.model.Announcement;
import com.bt.akh.poc.spring.repository.AnnouncementRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {

    private AnnouncementRepository announcementRepository;

    @Autowired
    public void setAnnouncementRepository(AnnouncementRepository announcementRepository) {
        this.announcementRepository = announcementRepository;
    }

    @Override
    public Announcement save(Announcement Announcement) {
        return announcementRepository.save(Announcement);
    }
    
    @Override
    public Iterable<?> save(List<Announcement> announcement) {
        return  announcementRepository.save(announcement);
    }

    @Override
    public Announcement findOne(String id) {
        return announcementRepository.findOne(id);
    }

    @Override
    public Iterable<Announcement> findAll() {
        return announcementRepository.findAll();
    }
    
    @Override
    public long count() {
        return announcementRepository.count();
    }

    @Override
    public void delete(Announcement Announcement) {
        announcementRepository.delete(Announcement);
    }
}
