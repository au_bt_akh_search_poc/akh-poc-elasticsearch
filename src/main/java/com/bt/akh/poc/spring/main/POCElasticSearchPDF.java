package com.bt.akh.poc.spring.main;

//import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
//import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bt.akh.poc.crawler.model.mdh.GetPolicyDocumentsResult;
import com.bt.akh.poc.crawler.service.GetJSONResultService;
//import com.bt.akh.poc.crawler.service.impl.GetEquityResultServiceImpl;
import com.bt.akh.poc.crawler.service.impl.GetPolicyDocumentsServiceImpl;
import com.bt.akh.poc.jsonfile.FileList;
import com.bt.akh.poc.spring.config.Config;
import com.bt.akh.poc.spring.model.PolicyDocuments;
import com.bt.akh.poc.spring.service.PolicyDocumentsService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class POCElasticSearchPDF {

	//@Autowired
	//private static ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private static PolicyDocumentsService policyDocumentsService;

	private GetPolicyDocumentsServiceImpl getPolicyDocumentsService;

	public static final Logger log = LoggerFactory.getLogger(POCElasticSearchPDF.class );

	private static String mdhEndPointURL;
	
	private static String policyDocumentsURL;

	private static String key2;

	List<GetPolicyDocumentsResult> hits;

	public POCElasticSearchPDF() {
		super();
		getPolicyDocumentsService = new GetPolicyDocumentsServiceImpl(mdhEndPointURL);
	}	


	public static void main(String[] args) throws URISyntaxException, Exception {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		try {			
			//ApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
			ctx.register(Config.class);
			ctx.refresh();
			policyDocumentsService = ctx.getBean(PolicyDocumentsService.class);

			log.info("Connected to Elasticsearch");

			mdhEndPointURL = ctx.getBeanFactory().resolveEmbeddedValue("${mdhEndPointPolicyDocuments.url}");

			log.info("mdhEndPointPolicyDocuments.url: "+ mdhEndPointURL);
			
			policyDocumentsURL = ctx.getBeanFactory().resolveEmbeddedValue("${pdfpolicyDocuments.url}");
			
			log.info("pdfpolicyDocuments.url: "+ policyDocumentsURL);

			POCElasticSearchPDF p = new POCElasticSearchPDF();

			log.info("After POCElasticSearchPDF p ");



			log.info("After addListedEquityBulk");

//			p.getPolicyDocuments();
			p.getFileList();

			log.info("After getPolicyDocuments");

			p.addPolicyDocuments();

			log.info("*** After all. ****");

		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally {
			log.info("Finally begin");
			ctx.close();
			log.info("Finally final");
		}

		log.info("Finish");

	}



	private void getFileList(){
		ObjectMapper mapper = new ObjectMapper();

		try {

			// Convert JSON string from file to Object
			//FileList fileList = mapper.readValue(new File("c:\\pdf_file list.txt"), FileList.class);
			FileList fileList = mapper.readValue(new File("pdf_file list.txt"), FileList.class);
			hits = fileList.getHits();

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void getPolicyDocuments() {
		// TODO Auto-generated method stub
		getPolicyDocumentsService = new GetPolicyDocumentsServiceImpl(mdhEndPointURL);
		hits = getPolicyDocumentsService.get(GetJSONResultService.restTemplate, key2).hits;

	}

	private void addPolicyDocuments() {
		// TODO Auto-generated method stub
		List<PolicyDocuments> policyDocumentsList = new ArrayList<PolicyDocuments>();

		for (GetPolicyDocumentsResult temp : hits) {
			log.info(temp.toString());
			String jcrPath = temp.getJcrPath();
			
			try {
			if (jcrPath != null && isPDF(jcrPath)){
				log.info("Inside isPDF IF");				
				
				jcrPath = policyDocumentsURL + jcrPath;
				log.info("jcrPath 1= " + jcrPath);
				
				//String fileContents = readContent(new File(jcrPath));
				String fileContents = readContent(jcrPath);
				log.info("fileContents = " + fileContents );
				
				//jcrPath = policyDocumentsURL + jcrPath;
				//log.info("jcrPath 2= " + jcrPath);
				
				if (temp.getJcrContent().getMetadata() == null){
				
					policyDocumentsList.add(new PolicyDocuments(temp.getJcrContent().getMetadata().getDcDescription(),temp.getJcrContent().getMetadata().getDcTitle(), temp.getJcrContent().getMetadata().getDcTitle(),temp.getJcrPath(),jcrPath,fileContents));
				}else {
					policyDocumentsList.add(new PolicyDocuments("","", "",temp.getJcrPath(),jcrPath,fileContents));
				}
				
			} else {
				log.info("Else is Not aPDF");
			}	}
			catch (Exception e) {
				log.info("Exception isPDF: "+e);
			}
		}
		policyDocumentsService.save(policyDocumentsList);
	}



	private boolean isPDF(String jcrPath) {
		// TODO Auto-generated method stub
		
		if (jcrPath.length()>4){
		return (jcrPath.substring(jcrPath.length()-4).equalsIgnoreCase(".pdf")) ? true : false;
		}
		return false;
	}


	//private static String readContent( File file ) throws Exception {
	private static String readContent( String pdfFile ) throws Exception {
		byte[] ba1= new byte[1024];
		int baLength;
		URL url1 =  new URL(pdfFile);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		log.info("Starting to read PDF: "+ pdfFile);
		
		try {
			URLConnection urlConn = url1.openConnection();
			
			
			//InputStream input = new BufferedInputStream( new FileInputStream( file ) );
			
			log.info("After url1.openConnection()");
			
			if (!urlConn.getContentType().equalsIgnoreCase("application/pdf")) {
				log.info("FAILED 1. [Sorry. This is not a PDF.]\n");
		      } else {
		    	  log.info("*** It's a PDF ***");
		    	  try {
			    	InputStream input = url1.openStream();
			    	log.info("*** After  url1.openStream() ***");
					//int read = -1;
			
					while ( (  baLength = input.read(ba1) ) != -1 ) {
						output.write(ba1);
					}
			
					input.close();
					
					log.info("PDF File read.");
					
			      } catch (ConnectException ce) {
		          System.out.println("FAILED 2 . [" + ce.getMessage() + "]\n");
		        }
		    	  
		      }  
				log.info("Before conversion base64");
		} catch (NullPointerException npe) {
		      System.out.println("FAILED 3. [" + npe.getMessage() + "]\n");
		    }
		log.info("\n");
		return Base64.encodeBase64String( output.toByteArray());
	}

	/*private void getAnnouncementList() {
		// TODO Auto-generated method stub
		getAnnouncementResultService = new GetAnnouncementResultServiceImpl(mdhEndPointAnnouncement);
		getAnnouncementResultList = getAnnouncementResultService.getList(GetJSONResultService.restTemplate);

	}*/


}
