package com.bt.akh.poc.spring.repository;

import com.bt.akh.poc.spring.model.PolicyDocuments;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PolicyDocumentsRepository extends ElasticsearchRepository<PolicyDocuments, String> {

   }
