package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.repository.ListedEquityRepository;
import com.bt.akh.poc.spring.model.ListedEquity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListedEquityServiceImpl implements ListedEquityService {

    private ListedEquityRepository ListedEquityRepository;

    @Autowired
    public void setListedEquityRepository(ListedEquityRepository ListedEquityRepository) {
        this.ListedEquityRepository = ListedEquityRepository;
    }

    @Override
    public ListedEquity save(ListedEquity ListedEquity) {
        return ListedEquityRepository.save(ListedEquity);
    }

    @Override
    public Iterable<?> save(List<ListedEquity> listedEquity) {
        return  ListedEquityRepository.save(listedEquity);
    }    
    
    @Override
    public ListedEquity findOne(String id) {
        return ListedEquityRepository.findOne(id);
    }

    @Override
    public Iterable<ListedEquity> findAll() {
        return ListedEquityRepository.findAll();
    }

    @Override
    public long count() {
        return ListedEquityRepository.count();
    }

    @Override
    public void delete(ListedEquity ListedEquity) {
        ListedEquityRepository.delete(ListedEquity);
    }
}
