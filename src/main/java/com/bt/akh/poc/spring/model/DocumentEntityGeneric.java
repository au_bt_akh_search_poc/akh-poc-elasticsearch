package com.bt.akh.poc.spring.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


public class DocumentEntityGeneric {

    @Id
	protected String id;
	
    protected String article_address;
   
    protected String article_author;
    
    protected String article_brand;
    
    protected Integer article_num_of_pages;
       
    protected String article_phonenum_alfa;

    protected Date article_publish_datetime;
    
    protected String article_stock_name;
        
    protected String article_stock_code;
    
    protected String article_website;
    
    protected String article_company_name;
    
    protected String article_body_content;    
    
    protected String cms_contributor;    
    
    protected String cms_copyright_owner;
    
    protected String cms_copyright;
        
    protected String cms_created_with;
        
    protected String cms_creator;    
    
    protected String cms_description;
    
    protected String cms_event_type;
    
    protected Date cms_expiry_datetime;
    
    protected String cms_heading;
    
    protected String cms_id;
    
    protected String cms_language;
    
    protected Date cms_off_time;
        
    protected Date cmd_on_time;
    
    protected String[] cms_tags;
    
    protected String cms_title;
    
    protected String cms_type;
    
    protected String cms_usage_restriction;
    
    protected String cms_usage_terms;
    
    protected String cms_version;
    
    protected String resource_author;
    
    protected String resource_body_content;
        
    protected boolean resouce_contains_audio;
    
    protected String resource_file_name;
    
    protected Date resource_lastupdated_datetime;
    
    protected Integer resource_number_of_bytes;
    
    protected Date resource_publish_datetime;
        
    protected String resource_title;    
    
    protected String resource_uri;
    
    protected String resource_url;
    

    protected String resource_base64_file;

    
    public DocumentEntityGeneric() {
    }
    
    


	public DocumentEntityGeneric(String article_address) {		
		this.article_address = article_address;
	}




	public DocumentEntityGeneric(String article_stock_name, String article_stock_code) {
		super();
		this.article_stock_name = article_stock_name;
		this.article_stock_code = article_stock_code;
	}




	public DocumentEntityGeneric(Date article_publish_datetime, String article_stock_code, String article_company_name,
			String cms_event_type, String cms_heading, String cms_id) {
		super();
		this.article_publish_datetime = article_publish_datetime;
		this.article_stock_code = article_stock_code;
		this.article_company_name = article_company_name;
		this.cms_event_type = cms_event_type;
		this.cms_heading = cms_heading;
		this.cms_id = cms_id;
	}
	
	public DocumentEntityGeneric(String article_address, String article_author, String article_brand,
			Integer article_num_of_pages, String article_phonenum_alfa, Date article_publish_datetime,
			String article_stock_name, String article_stock_code, String article_website, String article_company_name,
			String article_body_content, String cms_contributor, String cms_copyright_owner, String cms_copyright,
			String cms_created_with, String cms_creator, String cms_description, String cms_event_type,
			Date cms_expiry_datetime, String cms_heading, String cms_id, String cms_language, Date cms_off_time,
			Date cmd_on_time, String[] cms_tags, String cms_title, String cms_type, String cms_usage_restriction,
			String cms_usage_terms, String cms_version, String resource_author, String resource_body_content,
			boolean resouce_contains_audio, String resource_file_name, Date resource_lastupdated_datetime,
			Integer resource_number_of_bytes, Date resource_publish_datetime, String resource_title,
			String resource_uri, String resource_url, String resource_base64_file) {
		super();
		this.article_address = article_address;
		this.article_author = article_author;
		this.article_brand = article_brand;
		this.article_num_of_pages = article_num_of_pages;
		this.article_phonenum_alfa = article_phonenum_alfa;
		this.article_publish_datetime = article_publish_datetime;
		this.article_stock_name = article_stock_name;
		this.article_stock_code = article_stock_code;
		this.article_website = article_website;
		this.article_company_name = article_company_name;
		this.article_body_content = article_body_content;
		this.cms_contributor = cms_contributor;
		this.cms_copyright_owner = cms_copyright_owner;
		this.cms_copyright = cms_copyright;
		this.cms_created_with = cms_created_with;
		this.cms_creator = cms_creator;
		this.cms_description = cms_description;
		this.cms_event_type = cms_event_type;
		this.cms_expiry_datetime = cms_expiry_datetime;
		this.cms_heading = cms_heading;
		this.cms_id = cms_id;
		this.cms_language = cms_language;
		this.cms_off_time = cms_off_time;
		this.cmd_on_time = cmd_on_time;
		this.cms_tags = cms_tags;
		this.cms_title = cms_title;
		this.cms_type = cms_type;
		this.cms_usage_restriction = cms_usage_restriction;
		this.cms_usage_terms = cms_usage_terms;
		this.cms_version = cms_version;
		this.resource_author = resource_author;
		this.resource_body_content = resource_body_content;
		this.resouce_contains_audio = resouce_contains_audio;
		this.resource_file_name = resource_file_name;
		this.resource_lastupdated_datetime = resource_lastupdated_datetime;
		this.resource_number_of_bytes = resource_number_of_bytes;
		this.resource_publish_datetime = resource_publish_datetime;
		this.resource_title = resource_title;
		this.resource_uri = resource_uri;
		this.resource_url = resource_url;
		this.resource_base64_file = resource_base64_file;
	}


	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getArticle_address() {
		return article_address;
	}


	public void setArticle_address(String article_address) {
		this.article_address = article_address;
	}


	public String getArticle_author() {
		return article_author;
	}


	public void setArticle_author(String article_author) {
		this.article_author = article_author;
	}


	public String getArticle_brand() {
		return article_brand;
	}


	public void setArticle_brand(String article_brand) {
		this.article_brand = article_brand;
	}


	public Integer getArticle_num_of_pages() {
		return article_num_of_pages;
	}


	public void setArticle_num_of_pages(Integer article_num_of_pages) {
		this.article_num_of_pages = article_num_of_pages;
	}


	public String getArticle_phonenum_alfa() {
		return article_phonenum_alfa;
	}


	public void setArticle_phonenum_alfa(String article_phonenum_alfa) {
		this.article_phonenum_alfa = article_phonenum_alfa;
	}


	public Date getArticle_publish_datetime() {
		return article_publish_datetime;
	}


	public void setArticle_publish_datetime(Date article_publish_datetime) {
		this.article_publish_datetime = article_publish_datetime;
	}


	public String getArticle_stock_name() {
		return article_stock_name;
	}


	public void setArticle_stock_name(String article_stock_name) {
		this.article_stock_name = article_stock_name;
	}


	public String getArticle_stock_code() {
		return article_stock_code;
	}


	public void setArticle_stock_code(String article_stock_code) {
		this.article_stock_code = article_stock_code;
	}


	public String getArticle_website() {
		return article_website;
	}


	public void setArticle_website(String article_website) {
		this.article_website = article_website;
	}


	public String getArticle_company_name() {
		return article_company_name;
	}


	public void setArticle_company_name(String article_company_name) {
		this.article_company_name = article_company_name;
	}


	public String getArticle_body_content() {
		return article_body_content;
	}


	public void setArticle_body_content(String article_body_content) {
		this.article_body_content = article_body_content;
	}


	public String getCms_contributor() {
		return cms_contributor;
	}


	public void setCms_contributor(String cms_contributor) {
		this.cms_contributor = cms_contributor;
	}


	public String getCms_copyright_owner() {
		return cms_copyright_owner;
	}


	public void setCms_copyright_owner(String cms_copyright_owner) {
		this.cms_copyright_owner = cms_copyright_owner;
	}


	public String getCms_copyright() {
		return cms_copyright;
	}


	public void setCms_copyright(String cms_copyright) {
		this.cms_copyright = cms_copyright;
	}


	public String getCms_created_with() {
		return cms_created_with;
	}


	public void setCms_created_with(String cms_created_with) {
		this.cms_created_with = cms_created_with;
	}


	public String getCms_creator() {
		return cms_creator;
	}


	public void setCms_creator(String cms_creator) {
		this.cms_creator = cms_creator;
	}


	public String getCms_description() {
		return cms_description;
	}


	public void setCms_description(String cms_description) {
		this.cms_description = cms_description;
	}


	public String getCms_event_type() {
		return cms_event_type;
	}


	public void setCms_event_type(String cms_event_type) {
		this.cms_event_type = cms_event_type;
	}


	public Date getCms_expiry_datetime() {
		return cms_expiry_datetime;
	}


	public void setCms_expiry_datetime(Date cms_expiry_datetime) {
		this.cms_expiry_datetime = cms_expiry_datetime;
	}


	public String getCms_heading() {
		return cms_heading;
	}


	public void setCms_heading(String cms_heading) {
		this.cms_heading = cms_heading;
	}


	public String getCms_id() {
		return cms_id;
	}


	public void setCms_id(String cms_id) {
		this.cms_id = cms_id;
	}


	public String getCms_language() {
		return cms_language;
	}


	public void setCms_language(String cms_language) {
		this.cms_language = cms_language;
	}


	public Date getCms_off_time() {
		return cms_off_time;
	}


	public void setCms_off_time(Date cms_off_time) {
		this.cms_off_time = cms_off_time;
	}


	public Date getCmd_on_time() {
		return cmd_on_time;
	}


	public void setCmd_on_time(Date cmd_on_time) {
		this.cmd_on_time = cmd_on_time;
	}


	public String[] getCms_tags() {
		return cms_tags;
	}


	public void setCms_tags(String[] cms_tags) {
		this.cms_tags = cms_tags;
	}


	public String getCms_title() {
		return cms_title;
	}


	public void setCms_title(String cms_title) {
		this.cms_title = cms_title;
	}


	public String getCms_type() {
		return cms_type;
	}


	public void setCms_type(String cms_type) {
		this.cms_type = cms_type;
	}


	public String getCms_usage_restriction() {
		return cms_usage_restriction;
	}


	public void setCms_usage_restriction(String cms_usage_restriction) {
		this.cms_usage_restriction = cms_usage_restriction;
	}


	public String getCms_usage_terms() {
		return cms_usage_terms;
	}


	public void setCms_usage_terms(String cms_usage_terms) {
		this.cms_usage_terms = cms_usage_terms;
	}


	public String getCms_version() {
		return cms_version;
	}


	public void setCms_version(String cms_version) {
		this.cms_version = cms_version;
	}


	public String getResource_author() {
		return resource_author;
	}


	public void setResource_author(String resource_author) {
		this.resource_author = resource_author;
	}


	public String getResource_body_content() {
		return resource_body_content;
	}


	public void setResource_body_content(String resource_body_content) {
		this.resource_body_content = resource_body_content;
	}


	public boolean isResouce_contains_audio() {
		return resouce_contains_audio;
	}


	public void setResouce_contains_audio(boolean resouce_contains_audio) {
		this.resouce_contains_audio = resouce_contains_audio;
	}


	public String getResource_file_name() {
		return resource_file_name;
	}


	public void setResource_file_name(String resource_file_name) {
		this.resource_file_name = resource_file_name;
	}


	public Date getResource_lastupdated_datetime() {
		return resource_lastupdated_datetime;
	}


	public void setResource_lastupdated_datetime(Date resource_lastupdated_datetime) {
		this.resource_lastupdated_datetime = resource_lastupdated_datetime;
	}


	public Integer getResource_number_of_bytes() {
		return resource_number_of_bytes;
	}


	public void setResource_number_of_bytes(Integer resource_number_of_bytes) {
		this.resource_number_of_bytes = resource_number_of_bytes;
	}


	public Date getResource_publish_datetime() {
		return resource_publish_datetime;
	}


	public void setResource_publish_datetime(Date resource_publish_datetime) {
		this.resource_publish_datetime = resource_publish_datetime;
	}


	public String getResource_title() {
		return resource_title;
	}


	public void setResource_title(String resource_title) {
		this.resource_title = resource_title;
	}


	public String getResource_uri() {
		return resource_uri;
	}


	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}


	public String getResource_url() {
		return resource_url;
	}


	public void setResource_url(String resource_url) {
		this.resource_url = resource_url;
	}




	public void setResource_base64_file(String resource_base64_file) {
		this.resource_base64_file = resource_base64_file;
	}
	
	public String getResource_base64_file( ) {
		return resource_base64_file;
	}

	
}
