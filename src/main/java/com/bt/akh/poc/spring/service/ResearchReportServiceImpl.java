package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.repository.ResearchReportRepository;
import com.bt.akh.poc.spring.model.ResearchReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResearchReportServiceImpl implements ResearchReportService {

    private ResearchReportRepository ResearchReportRepository;

    @Autowired
    public void setResearchReportRepository(ResearchReportRepository ResearchReportRepository) {
        this.ResearchReportRepository = ResearchReportRepository;
    }

    @Override
    public ResearchReport save(ResearchReport ResearchReport) {
        return ResearchReportRepository.save(ResearchReport);
    }

    @Override
    public ResearchReport findOne(String id) {
        return ResearchReportRepository.findOne(id);
    }

    @Override
    public Iterable<ResearchReport> findAll() {
        return ResearchReportRepository.findAll();
    }
    
    @Override
    public long count() {
        return ResearchReportRepository.count();
    }

    @Override
    public void delete(ResearchReport ResearchReport) {
        ResearchReportRepository.delete(ResearchReport);
    }
}
