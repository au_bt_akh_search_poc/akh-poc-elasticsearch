package com.bt.akh.poc.spring.service;

import java.util.List;

import com.bt.akh.poc.spring.model.Announcement;

public interface AnnouncementService {
	
	Announcement save(Announcement article);

	Iterable<?> save(List<Announcement> announcementList);
	
	Announcement findOne(String id);

    Iterable<Announcement> findAll();

    long count();

    void delete(Announcement article);
    
    
    
    
}
