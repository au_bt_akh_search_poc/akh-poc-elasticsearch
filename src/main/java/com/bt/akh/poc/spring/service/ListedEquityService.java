package com.bt.akh.poc.spring.service;

import java.util.List;

import com.bt.akh.poc.spring.model.ListedEquity;

public interface ListedEquityService {
	
	ListedEquity save(ListedEquity article);
	
	Iterable<?> save(List<ListedEquity> listedEquityList);

	ListedEquity findOne(String id);

    Iterable<ListedEquity> findAll();

    long count();

    void delete(ListedEquity article);
}
