package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.repository.PolicyDocumentsRepository;
import com.bt.akh.poc.spring.model.PolicyDocuments;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PolicyDocumentsServiceImpl implements PolicyDocumentsService {

    private PolicyDocumentsRepository PolicyDocumentsRepository;

    @Autowired
    public void setPolicyDocumentsRepository(PolicyDocumentsRepository PolicyDocumentsRepository) {
        this.PolicyDocumentsRepository = PolicyDocumentsRepository;
    }

    @Override
    public PolicyDocuments save(PolicyDocuments PolicyDocuments) {
        return PolicyDocumentsRepository.save(PolicyDocuments);
    }
    
    @Override
    public Iterable<?> save(List<PolicyDocuments> PolicyDocuments) {
        return PolicyDocumentsRepository.save(PolicyDocuments);
    }

    @Override
    public PolicyDocuments findOne(String id) {
        return PolicyDocumentsRepository.findOne(id);
    }

    @Override
    public Iterable<PolicyDocuments> findAll() {
        return PolicyDocumentsRepository.findAll();
    }

    @Override
    public long count() {
        return PolicyDocumentsRepository.count();
    }

    @Override
    public void delete(PolicyDocuments PolicyDocuments) {
        PolicyDocumentsRepository.delete(PolicyDocuments);
    }
}
