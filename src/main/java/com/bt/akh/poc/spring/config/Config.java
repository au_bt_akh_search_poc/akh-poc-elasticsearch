package com.bt.akh.poc.spring.config;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.bt.akh.poc.spring.repository")
@ComponentScan(basePackages = { "com.bt.akh.poc" })
@PropertySource("classpath:elasticsearchconfig.properties")
public class Config {

    @Value("${elasticsearch.home:/usr/local/Cellar/elasticsearch/2.3.2}")
    private String elasticsearchHome;    
    
  	@Value("${elasticsearch.ip}")
  	private String ip;
  	
  	@Value("${elasticsearch.port}") 	
  	private String port;
  	
  	@Value("${elasticsearch.ip2}")
  	private String ip2;
  	
  	@Value("${elasticsearch.port2}") 	
  	private String port2;
  	
  	@Value("${mdhEndPoint.url}") 	
  	private String mdhEndPointURL;
  	
  	@Value("${mdhEndPointAnnouncement.url}") 	
  	private String mdhEndPointAnnouncement;
  	
    private static Logger logger = LoggerFactory.getLogger(Config.class);

    @Bean
    public Client client() {
        try {
            final Path tmpDir = Files.createTempDirectory(Paths.get(System.getProperty("java.io.tmpdir")), "elasticsearch_data");

            // @formatter:off

            final Settings.Builder elasticsearchSettings =
                    Settings.settingsBuilder().put("http.enabled", "false")
                                              .put("path.data", tmpDir.toAbsolutePath().toString())
                                              .put("path.home", elasticsearchHome)
                                              ;
            // @formatter:on
            logger.debug(tmpDir.toAbsolutePath().toString());
            //return new NodeBuilder().local(true).settings(elasticsearchSettings.build()).node().client();
            return new TransportClient.Builder().settings(elasticsearchSettings).build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(ip), Integer.parseInt(port))).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(ip2), Integer.parseInt(port2)));
        } catch (final IOException ioex) {        	
            logger.error("Cannot create temp dir", ioex);
            throw new RuntimeException();
        }
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchTemplate(client());
    }
    
  //To resolve ${} in @Value
  	@Bean
  	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
  		return new PropertySourcesPlaceholderConfigurer();
  	}
  	
  
}