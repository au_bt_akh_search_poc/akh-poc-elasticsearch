package com.bt.akh.poc.spring.service;

import java.util.List;

import com.bt.akh.poc.spring.model.PolicyDocuments;

public interface PolicyDocumentsService {
	
	PolicyDocuments save(PolicyDocuments article);
	
	Iterable<?> save(List<PolicyDocuments> article);

	PolicyDocuments findOne(String id);

    Iterable<PolicyDocuments> findAll();

    long count();

    void delete(PolicyDocuments article);
}
