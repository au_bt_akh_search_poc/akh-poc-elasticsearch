package com.bt.akh.poc.spring.repository;

import com.bt.akh.poc.spring.model.Announcement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnouncementRepository extends ElasticsearchRepository<Announcement, String> {

}
