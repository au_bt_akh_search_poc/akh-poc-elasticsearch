package com.bt.akh.poc.spring.service;


import com.bt.akh.poc.spring.model.ResearchReport;

public interface ResearchReportService {
	
	ResearchReport save(ResearchReport article);

	ResearchReport findOne(String id);

    Iterable<ResearchReport> findAll();

    long count();

    void delete(ResearchReport article);
}
