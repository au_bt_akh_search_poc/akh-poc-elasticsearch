package com.bt.akh.poc.spring.main;

import java.util.Date;
import java.util.List;

import org.quartz.Job;
//import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import com.bt.akh.poc.crawler.model.mdh.GetEquityResult;
import com.bt.akh.poc.crawler.service.GetJSONResultService;
import com.bt.akh.poc.crawler.service.impl.EquityServiceImpl;
import com.bt.akh.poc.spring.config.Config;
import com.bt.akh.poc.spring.model.ListedEquity;
import com.bt.akh.poc.spring.service.ListedEquityService;

public class IndexingJob implements Job {
	
	public static final Logger log = LoggerFactory.getLogger(IndexingJob.class);

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;
	
	@Autowired
	private static ListedEquityService listedEquityService;
	
	private EquityServiceImpl getEquityResultService;
	
	GetEquityResult getEquityResult;
	
	private String mdhEndPointURL;
	
	public IndexingJob() {
		super();
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		try {			
			ctx.register(Config.class);
			ctx.refresh();

			listedEquityService = ctx.getBean(ListedEquityService.class);
			mdhEndPointURL = ctx.getBeanFactory().resolveEmbeddedValue("${mdhEndPoint.url}");
			log.info("mdhEndPointIp: "+ mdhEndPointURL);
			
			getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
		}
		finally {
			log.info("Finally begin");
			ctx.close();
			log.info("Finally final");
		}
	}

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {

			//POCElasticSearch p = new POCElasticSearch();
			
			//p.getEquity();
			getEquity("BHP");
			
			log.info("After getEquity");
		
			//p.addListedEquity();
			addListedEquity();
			
			log.info("After addListedEquity");
			

		
		log.info("Finish");

	}
	
	public void getEquity(String key) {
//		getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
//		getEquityResult = getEquityResultService.get(GetJSONResultService.restTemplate).GetEquityResult;
		
		getEquityResult = getEquityResultService.get(GetJSONResultService.restTemplate, key).GetEquityResult;	
		
	}
	
	public List<String> getEquityList( ) {
//		getEquityResultService = new EquityServiceImpl(mdhEndPointURL);
		List<String> equitList = getEquityResultService.getList(GetJSONResultService.restTemplate);		
		return equitList;
	}
	
	public void addListedEquity() {
		
		//ListedEquity listedEquity = new ListedEquity(getEquityResult.getStockName(), getEquityResult.getStockCode());
		ListedEquity listedEquity = new ListedEquity(getEquityResult.getStockName(), getEquityResult.getStockCode(), new Date());
		listedEquityService.save(listedEquity);
	}

}
