package com.bt.akh.poc.spring.service;

import com.bt.akh.poc.spring.repository.WebArticlesRepository;
import com.bt.akh.poc.spring.model.WebArticles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebArticlesServiceImpl implements WebArticlesService {

    private WebArticlesRepository WebArticlesRepository;

    @Autowired
    public void setWebArticlesRepository(WebArticlesRepository WebArticlesRepository) {
        this.WebArticlesRepository = WebArticlesRepository;
    }

    @Override
    public WebArticles save(WebArticles WebArticles) {
        return WebArticlesRepository.save(WebArticles);
    }

    @Override
    public WebArticles findOne(String id) {
        return WebArticlesRepository.findOne(id);
    }

    @Override
    public Iterable<WebArticles> findAll() {
        return WebArticlesRepository.findAll();
    }

    @Override
    public long count() {
        return WebArticlesRepository.count();
    }

    @Override
    public void delete(WebArticles WebArticles) {
        WebArticlesRepository.delete(WebArticles);
    }
}
