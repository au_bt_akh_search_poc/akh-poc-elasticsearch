package com.bt.akh.poc.spring.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.bt.akh.poc.spring.model.WebArticles;

@Repository
public interface WebArticlesRepository extends ElasticsearchRepository<WebArticles, String> {

    }
