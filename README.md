# Real Time Integration between Elasticsearch / AEM / Other Source Data systems using Java/Spring #


The aim of this POC is to build the functional architecture and demo to be used in the real time integration that will allow to populate 4 different indexes in Elasticsearch with information from AEM and other source system data providers.

Information will be accessed finally by the end user in a React JS front end UI, with a Google style search screen where the user will type any term in the UI and then  a service call from React will verify in Elasticsearch all occurrences of that term either at index level or page level, or within a PDF document.

The software component exposed here ### The Java Troller App ###,  will deal with the integration among Elasticsearch, AEM (Adobe Experience Manager), MDH (Market Data Hub) and other platforms only. It will be scheduled by using Java/Quartz several times per day to go to every source system, connect to them by using RESTFull services, extract info from them. Then by using Java and the Spring framework it will validate and transform the information and finally by using the Spring/Elasticsearch API it will connect to Elasticsearch and update relevant indexes with basic data and URI where the actual document is store for the UI to display the actual doc/page if required by the end user.



## ****Java Troller App ##

### Background ###

Effort was put in order to find the best solution to populate ES from the different data sources for this POC within the timeframe restriction. Due to the different type of data sources I found that there was no one solution in the market able to deal  with all the different sources types like AEM,  JSon formats, PDFs, plane files and others.  Down that path I also found that java has got the versatility to connect to ES and update or create indexes. First version of the app managed to write to ES with basic functionality but there was a level of complexity to create or update indexes. Later on I found that Spring has a library that reduced the complexity of communication with ES. We build out first app, it was a success and then we evolved that first approach to a better architecture in order to grow in an organized manner.

I was able to connect to all data sources, crawl them up, extract the information transform it and write the important key elements to ES

### Architecture ###

![ES_Java_Spring_Integration_01.jpg](https://bitbucket.org/repo/AKykjB/images/1123769278-ES_Java_Spring_Integration_01.jpg)



### ****Package Structure ###

![ES_Java_Spring_Integration_02.jpg](https://bitbucket.org/repo/AKykjB/images/52233072-ES_Java_Spring_Integration_02.jpg)


****Explanations of each package

* com.bt.akh.poc.crawler.model.mdh holds data model of fetching result from MDH API
* com.bt.akh.poc.crawler.service holds the general interface for getting JSON Result
* com.bt.akh.poc.crawler.service.impl holds classes implementing the service interface, which includes:

![ES_Java_Spring_Integration_03.jpg](https://bitbucket.org/repo/AKykjB/images/2018576112-ES_Java_Spring_Integration_03.jpg)

* com.bt.akh.poc.jsonfile holds FileList.txt which may need to be exposed as REST API later by Amit.
* com.bt.akh.poc.spring.config holds config class for Spring.
* com.bt.akh.poc.spring.main holds the main classes to be called directly as a standalone application.
* * POCElasticSearch fetch 1)Announcement data, 2) list of equity names, and 3) all the equities’ detail, and then add to Elastic engine
* * POCElasticSearchPDF fetch all file names from FileList.txt, and pick only pdf file names to fetch PDF content, and then add to Elastic engine
* * POCESMain calls IndexingJob as Quartz job regularly (frequency can be adjusted)
* * * load data from API (BHP equity in this example) 
* * * and add to Elastic engine
* com.bt.akh.poc.spring.model holds data model (index-type-document 3 levels) to be added to Elastic Search Engine
* com.bt.akh.poc.spring.repository holds Repository interfaces Elastic Search client service
* com.bt.akh.poc.spring.service holds client service interfaces and implementations to handle data manipulation with Elastic Search Engine


### Running the app ###

****Env Setup

Java JDK 1.8

Connection to Elasticsearch server (terminal client usually port 9300)
Connections to mdhEndPoint, mdhEndPointAnnouncement, mdhEndPointPolicyDocuments and pdfpolicyDocuments and points.

1. The list of runnable files together with separate folders of dependencies have to be copied to a new folder.

1) runnable files
akh-poc-elasticsearch.jar
akh-poc-elasticsearchPDF.jar 
akh-poc-POCESMain.jar

2) dependencies
akh-poc-elasticsearchPDF_lib
akh-poc-elasticsearch_lib
akh-poc-POCESMain_lib

3) properties and data files
elasticsearchconfig.properties
pdf_file list.txt


Structure like this:

![ES_Java_Spring_Integration_04.jpg](https://bitbucket.org/repo/AKykjB/images/2900161837-ES_Java_Spring_Integration_04.jpg)

2. Edit elasticsearchconfig.properties file and update  next properties:

* elasticsearch.ip=localhost
* elasticsearch.port=9300
* elasticsearch.ip2=localhost
* elasticsearch.port2=9300
* mdhEndPoint.url=http://akh_mdh.web.btfin.com
* mdhEndPointAnnouncement.url=http://akh_mdh.web.btfin.com/MS_Equities.svc
* mdhEndPointPolicyDocuments.url=https://s3-ap-southeast-2.amazonaws.com
pdfpolicyDocuments.url=http://akh.web.btfin.com/api/v1

****Running the app

1. To populate Equities and announcements, in a command line window (DOS) please write:

Java –jar akh-poc-elasticsearch.jar

2. To populate pdf policy documents, in a command line window (DOS) please write:

Java –jar akh-poc-elasticsearchPDF.jar

### ****Checking the results ###

1. check equities detail, http://au21calp0026:2345/commonsearch/_search?&pretty 
![ES_Java_Spring_Integration_05.jpg](https://bitbucket.org/repo/AKykjB/images/2877344521-ES_Java_Spring_Integration_05.jpg)
2. Check announcements, http://au21calp0026:2345/commonsearch/announcement/_search?&pretty

![ES_Java_Spring_Integration_06.jpg](https://bitbucket.org/repo/AKykjB/images/3781929671-ES_Java_Spring_Integration_06.jpg)

Check PDF details, http://au21calp0026:2345/commonsearch/_search?q=CPB.pdf&pretty
![ES_Java_Spring_Integration_07.jpg](https://bitbucket.org/repo/AKykjB/images/1406335451-ES_Java_Spring_Integration_07.jpg)

### To be Enhanced ###

Currently the save to ES function is only appending as we don’t keep id, if we need to control this or update original data (not appending), then we need to use a mechanism to generate and manage IDs.
The code is just for POC and functionally workable, could refactor to better structure.


### Conclusion ###
After building our own in house approach to this challenge I recommend the versatility that the combination of Java/Spring brings to Elastic search. We managed to crawl very different sources of information, write the key elements to ES and if needed  we can tailor this app to work as a search engine by  tailoring searches needed by web and mobile apps and reducing the complexity of doing these search from UIs.


## Spring Data Elasticsearch
- [Introduction to Spring Data Elasticsearch](http://www.baeldung.com/spring-data-elasticsearch-tutorial)

### Build the Project with Tests Running
```
mvn clean install
```

### Run Tests Directly
```
mvn test
```