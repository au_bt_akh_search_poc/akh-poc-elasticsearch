﻿{  
   "success":true,
   "results":110,
   "total":110,
   "offset":0,
   "hits":[  
      {  
         "jcr:path":"/content/dam/btap-poc/banner.png",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/CFM Information Guide.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"New Description for Testing",
               "dc:title":"New Title for Testing"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Create an Ad Hoc Customer Query.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/lost_super.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Lost superannuation benefits can be traced through the ATO\u2019s myGov portal which will find lost benefits held by superannuation funds or by the ATO.",
               "dc:title":"Lost Superannuation"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/How To Resolve Common Scanning  Issues.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Completing RoAs in BPM Workaround.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Chant West Scenario Guide.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"sample stylesheet for bankers trust",
               "dc:title":"stylesheet"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/ATP Structure Diagrams.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Talks about Advice Transformation Organisation Chart, Advice Transformation Program Workstreams at BT Financial Group. It breaks down into: Design Council members, Steering committee members and program streams",
               "dc:title":"ATP Structure Diagram"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Create a Manual Workflow without change.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/con_con.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Concessional contributions are those which a tax deduction has been claimed.  They are included in the taxable component of the fund and are preserved benefits.\nThe document further talks about Excess concessional contributions, Refund of excess concessional contributions and Treatment of non concessional contributions.",
               "dc:title":"Concessional Contributions"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Create a Personlised Inbox or History.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/ATP Digitisation Schedule.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"ATP Digitisation Schedule (Advice Transformation Program)\nTalks about the schedule for various Australian states",
               "dc:title":"ATP Digitisation Schedule"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Add a file note.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"BPM System \u2018How To\u2019 Information Sheet v0.2\nHow to Add a File Note\nBPM > All Screens\nFinancial Planner",
               "dc:title":"BPM System \u2018How To\u2019 Information Sheet v0.2"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/ATP Q and A.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"The Advice Transformation Program (ATP) is dedicated to simplifying and streamlining the way you do business in order to provide you with more customer-facing time.\nA number of the questions and answers in this document refer to the two new systems being implemented. These are explained below:\n1. Business Process Management\n2. Client File Management (FileNet)\n\nINDEX\nADVICE DOCUMENTATION .............................................................................................. 13\nADVICE PROCESS ............................................................................................................ 14\nADVISERNET GAIN (ANG) & FIREFLY ............................................................................. 17\nATP .................................................................................................................................... 17\nAUDIT ................................................................................................................................. 19\nBT ADVISER SERVICES ................................................................................................... 19\nBUSINESS PROCESS MANAGEMENT SYSTEM ............................................................. 20\nBUSINESS PROCESS MANAGEMENT WORKFLOW ...................................................... 22\nBUSINESS PROCESS MANAGEMENT ADVICE PROCESS / STRATEGY ...................... 26\nBUSINESS PROCESS MANAGEMENT TECHNOLOGY ................................................... 30\nCLIENT FILE MANAGEMENT DIGITISATION ................................................................... 32\nCLIENT FILE MANAGEMENT SYSTEM ............................................................................ 38\nCUSTOMERS ..................................................................................................................... 40\nFLEXIBLE WORKING ARRANGEMENTS ......................................................................... 41\nLOCATION ......................................................................................................................... 42\nOPERATING MODEL ......................................................................................................... 42\nOPERATING RHYTHM ...................................................................................................... 44\nPARAPLANNING ............................................................................................................... 45\nPARTNERSHIPS ................................................................................................................ 46\nPLANNER ASSISTANT ROLE ........................................................................................... 47\nPLANNER ROLE ................................................................................................................ 52\nREGIONAL MANAGER ROLE ........................................................................................... 54\nSUBSEQUENT ADVICE ..................................................................................................... 55\nSUPPORT .......................................................................................................................... 55\nTEAM ASSISTANT ROLE .................................................................................................. 56\nTEAM LEADER ROLE ....................................................................................................... 58\nTECHNOLOGY ................................................................................................................... 59\nTRAINING .......................................................................................................................... 61\nWINDOWS 8 AND TABLETS ............................................................................................. 62\nAPPENDIX \u2013 SERVICE LEVEL AGREEMENTS WITHIN BUSINESS PROCESS MANAGEMENT .................................................................................................................. 63",
               "dc:title":"Advice Transformation Program Q&A v14"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/SMSFInsCampaign.png",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/BPM Screen Reference Guide.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"The guide contains the screen by screen view of the following advice processes:\n Initial Advice\n Ongoing Advice\n Ad Hoc Advice",
               "dc:title":"Business Process Management (BPM) Screen Reference Guide V10"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Chant West Learning Byte.mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/ATP Overview.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Advice Transformation Program Overview\nProgram overview around consistent planner support model, Electronic client file management (CFM) and Digital Workflow System (BPM)",
               "dc:title":"Advice Transformation Program Overview"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/CFM Quick Reference Guide - with BPM - Final.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/BPM Service Level Agreements.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Advice Transformation Program BPM Service Level Agreements v 01\n\nService Level Agreements within Business Process Management:\n1. Seeing the Value of Financial Planning\n2. Conduct Discovery\n3. Bring Your Plan to Life\n4. Putting Your Plan Into Action",
               "dc:title":"Advice Transformation Program BPM Service Level Agreements v 01"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Create Time Critical Advice.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/How To Manage Inflight Cases.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Chant West - Updated Naming Conventions.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"BT Portfolio SuperWrap+\nAsgard eWRAP\nBT Portfolio SuperWrap+\nSuper/ Pension\nInvestment",
               "dc:title":"Model Portfolios"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Change a task priority.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/BPM March Release Powerpoint.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"BPM Release March 2016 Webinar\nPresenter: Vanessa Vu\nAgenda is as below:\nUpdated Initial Advice Process\n1. Updated Ongoing Advice Process\n2. Changes in detail\n3. Important Points\n4. Where to go for Help",
               "dc:title":"BPM Release March 2016Webinar"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Add a Replacement Policy Checklist.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"How to Add a Replacement Policy Checklist (BPM System \u2018How To\u2019 Information Sheet v0.5)\nThis is a policy check list for:\nFinancial Planners\nPlanner assistants",
               "dc:title":"How to Add a Replacement Policy Checklist"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/A-Z of ATP.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"This document lists everything you need to know about ATP. \nThe following is what it lists:\n1. BPM\n2. CFM\n3. ATP\n4. Digitisation\n5. Planner support model\n6. Sprints\n7. User experience\n8. Workflow",
               "dc:title":"A-Z of ATP"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Hint and Tips  - Blanking out TFN.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Complete a Clarification Rework request from Paraplanning.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/E-signatures How To Guide V1.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"",
               "dc:title":"Microsoft Word - 20160329 E-signatures How To Guide V1 (2).docx"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Cancel an Advice Request.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"How to Cancel an Advice Request for the following roles:\nFinancial Planner\nPlanner Assistants\nTeam Leaders",
               "dc:title":"How to Cancel an Advice Request - BPM System \u2018How To\u2019 Information Sheet v0.2"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/Financial Planner Participant Guide.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/technical/tax_ded.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Employers and certain individuals are able to claim tax deductions for superannuation contributions. Employers can claim deductions for contributions made for employees. Individuals can claim deductions for their own contributions if they meet certain conditions. \nDocuments talks about:\nContributions for employees\nMember deductible contributions\nMaximum earnings as employee condition\nImportant Points\nTips\nExample\nNotice of Intent to deduct conditions",
               "dc:title":"Tax deductions for contributions"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/policy/Introduction to Compliance.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":""
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/policy/IDandPromo Policy Update July 2016 Recorded_Comliance_Training.pptx",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"Slide 1"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/policy/Data Collection Policy Update July 2016 Recorded_Compliance_Training.pptx",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"Slide 1"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/policy/Compliance-Checkbox.jpg",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Business man with check boxes over navy blue background",
               "dc:title":"Business Man With Checkboxes"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/policy/Your Responsibilities Policy Update July 2016 recorded_Compliance_Training.pptx",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"Slide 1"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/new-to-investing-in-shares.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/super2.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/estate1.jpg",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"ä½\u008få®\u0085ã\u0082¤ã\u0083¡ã\u0083¼ã\u0082¸"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/estate2.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/estate3.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/1335740292856.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/bonus-shares-issued.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/905997-self-managed-super.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/210_SMSF.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/simple_img_4.jpg",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"OLYMPUS DIGITAL CAMERA         "
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/Stocks-and-Shares.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/shares2.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/How_to.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":""
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/super1.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/how-to/Shares.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/tasks/CPB.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/tasks/PPRF.xsn",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/alert/sample_alert2.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":""
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/alert/sample_alert1.docx",
         "jcr:content":{  
            "metadata":{  
               "dc:description":""
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/marketing/eofy-soc-media-messages.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/marketing/13183-0416tc Magnitude - Co-Branding Guidelines WEB.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/marketing/EOFY 2016 Campaign Template.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"Slide 1"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BT Risk Profiler Full Booklet - Bank of Melbourne.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/eQR to Morningstar transition training webinar for BFP staff.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Risk Profiler (Questions) Securitor V1.2a.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Off-APL form questions.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Module 7a (compliance).mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Off-APL Reasonable Product Investigation Guide.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/75_1446196474.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Securitor Off-APL Product Approval Guidelines.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Westpac Retail Entitlement Offer - comms to adviser 2015.10.21_BTGL.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Off APL BTGL V2.0.mov",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Industry-Research-icon.png",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Module 1a (Educate).mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Howto.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Morningstar - WBC Rights Offer - 19 October 2015.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Risk Profiler (Questions) Bank of Melb FP V1.2.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Module 6a (validation).mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/ThematicResearch.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/listed_equities.png",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/MarketInsights.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Risk Profiler (Full Booklet) Bank of Melb FP V1.2.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/market-insights.jpg",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Risk Profiler (Full Booklet) St George FP V1.2a.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BT Risk Profiler Questions Booklet.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Drawing on PDF V1.0.mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/eQR to Morningstar transition training webinar for BTGL.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BFP Off-APL Product Approval Guidelines.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Sec Off-APL Standalone Product Details Form (editable PDF).pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Module Intro(a).mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BTGL-Mandatory implementation of Risk Profiler extended.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BFP Off-APL Application Form (editable PDF).pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Role play - completing the questions.mp4",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Off-APL process flowchart.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Banker Sales Process",
               "dc:title":"Off APL Product Approval"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/CapitalMarketsUpdate.pdf",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"MArch Quarter 2016                         Capital Markets Update"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/Risk Profiler (Full Booklet) BT Select V1.2a.pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/docs/research/BFP Off-APL Standalone Product Details Form (editable PDF).pdf",
         "jcr:content":{  
            "metadata":{  

            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/man-with-digital-tablet-1280px.jpg",
         "jcr:content":{  
            "metadata":{  
               "dc:description":"Man hands using tablet. Virtual elements near computer. City as backdrop",
               "dc:title":"Man hands using tablet. Virtual elements near computer"
            }
         }
      },
      {  
         "jcr:path":"/content/dam/btap-poc/investor_phones_x3.png",
         "jcr:content":{  
            "metadata":{  
               "dc:title":"Clone from Pano"
            }
         }
      }
   ]
}